# ✨ Our Features

{% hint style="info" %}
**GitBook tip:** A succinct video overview is a great way to introduce folks to your product. Embed a Loom, Vimeo or YouTube video and you're good to go! We love this video from the fine folks at Loom as a perfect example of a succinct feature overview.
{% endhint %}

## Awesome Feature One

Get amazing things done with awesome feature one. But remember that awesome feature two and three exist too. In fact, Awesome Product is full of awesome features.

![](https://images.unsplash.com/photo-1555774698-0b77e0d5fac6?crop=entropy\&cs=tinysrgb\&fm=jpg\&ixid=MnwxOTcwMjR8MHwxfHNlYXJjaHwyfHxhcHB8ZW58MHx8fHwxNjYwNTgzMzQz\&ixlib=rb-1.2.1\&q=80)

## Awesome Feature Two 2

Get amazing things done with awesome feature two. But remember that awesome feature one and three exist too. In fact, Awesome Product is full of awesome features.

![](https://images.unsplash.com/photo-1569144157591-c60f3f82f137?crop=entropy\&cs=tinysrgb\&fm=jpg\&ixid=MnwxOTcwMjR8MHwxfHNlYXJjaHwxfHxmZWF0dXJlfGVufDB8fHx8MTY2MDU4MzM1OQ\&ixlib=rb-1.2.1\&q=80)

{% swagger src="../.gitbook/assets/api_test.json" path="/pet/{petId}/uploadImage" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet" method="put" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet/findByStatus" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet/findByTags" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet/{petId}" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet/{petId}" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/pet/{petId}" method="delete" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/store/inventory" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/store/order" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/store/order/{orderId}" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/store/order/{orderId}" method="delete" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/createWithList" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/{username}" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/{username}" method="put" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/{username}" method="delete" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/login" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/logout" method="get" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user/createWithArray" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}

{% swagger src="../.gitbook/assets/api_test.json" path="/user" method="post" %}
[api_test.json](../.gitbook/assets/api_test.json)
{% endswagger %}
